const auth = require("../auth");
const Product = require("../models/Product.js");
const User = require("../models/User.js");
const cloudinary = require("../utilities/cloudinary.js");
// import dotenv from "dotenv";
// dotenv.config({ path: "../controllers/productController.js" });
// require("dotenv").config();
module.exports.addProduct = async (reqBody) => {
  try {
    const result = await cloudinary.uploader.upload(reqBody.image, {
      folder: "products",
    });

    let newProduct = new Product({
      name: reqBody.name,
      description: reqBody.description,
      category: reqBody.category,
      supply: reqBody.supply,
      price: reqBody.price,
      image: {
        public_id: result.public_id,
        url: result.secure_url,
      },
    });

    await newProduct.save();

    return true;
  } catch {
    return false;
  }
};

module.exports.getAllProducts = async () => {
  return await Product.find({}).then((res) => {
    return res;
  });
};

module.exports.getProduct = (reqParams) => {
  return Product.findById(reqParams.productId).then((res) => {
    return res;
  });
};

module.exports.getAllActiveProducts = async () => {
  return await Product.find({ isActive: true }).then((res) => {
    return res;
  });
};

module.exports.updateProduct = async (reqParams, reqBody) => {
  try {
    if (reqBody.image != null) {
      const currentProduct = await Product.findById(reqParams.productId);
      const imgId = currentProduct.image.public_id;
      // console.log(imgId);
      if (imgId) {
        await cloudinary.uploader.destroy(imgId);
      }

      const newImage = await cloudinary.uploader.upload(reqBody.image, {
        folder: "products",
      });
      reqBody.image = {
        public_id: newImage.public_id,
        url: newImage.secure_url,
      };
      console.log(reqBody);
    }

    await Product.findByIdAndUpdate(reqParams.productId, reqBody);
    return true;
  } catch {
    return false;
  }
};

module.exports.archiveProduct = (reqParams) => {
  return this.updateProduct(reqParams, { isActive: false });
};

module.exports.activateProduct = (reqParams) => {
  return this.updateProduct(reqParams, { isActive: true });
};

module.exports.getOrdersToShip = async () => {
  //{productName, supply, qtyOfOrders}
  // try {
  let allProducts = await Product.find({});
  let allProductsWithOrder = allProducts.filter(
    (product) => product.orders.length > 0
  );
  //console.log(allProductsWithOrder.length);
  let ordersSummary = [];
  if (allProductsWithOrder.length > 0) {
    for (let i = 0; i < allProductsWithOrder.length; i++) {
      ordersSummary.push({
        productName: allProductsWithOrder[i].name,
        supply: allProductsWithOrder[i].supply,
        qtyOfOrders: allProductsWithOrder[i].orders.reduce(
          (x, y) => y.quantity + x,
          0
        ),
      });
      console.log(ordersSummary);
    }
  }
  return ordersSummary;
  // } catch {
  //   return false;
  // }
};
