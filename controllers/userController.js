const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const User = require("../models/User.js");

/*Business Logic of checking out order from cart
  1. Compare req.body in the user's cart
    • if req.body.productId is in the cart of the user get quantity and update req.body attaching the quantity
    • if otherwise, return false (Product is not in the cart)
  2. Use the updated req.body to call the checkOutOrder function
    • if no error was catched and the order is processed based on the logic of checkOutOrder, proceed.
    • if otherwise, return false
  3. Remove products from the cart that have been checked-out. 
*/

module.exports.checkOutOrderFromCart = async (reqId, reqBody) => {
  let checkedOutStatus = await this.checkOutOrder(reqId, reqBody);
  if (checkedOutStatus) {
    for (let i = 0; i < reqBody.length; i++) {
      await this.removeProductFromCart(reqId, reqBody[i].productId);
    }
    return true;
  } else {
    return false;
  }
};

module.exports.checkEmailExists = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result != null) {
      return true;
    } else {
      return false;
    }
  });
};

module.exports.registerUser = async (reqBody) => {
  try {
    reqBody.password = bcrypt.hashSync(reqBody.password, 10);
    let user = new User(reqBody);
    await user.save();
    return true;
  } catch {
    return false;
  }
};

module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};

module.exports.getDetails = (reqId) => {
  return User.findById(reqId).then((result, err) => {
    if (err) {
      // console.log(err);
      return false;
    } else {
      result.password = "";
      return result;
    }
  });
};

const getTotalOrderAmount = async (reqBody) => {
  let totalAmount = 0;
  for (let i = 0; i < reqBody.length; i++) {
    let prodDetails = await Product.findById(reqBody[i].productId);

    if (reqBody.status == null || reqBody.status == "To Ship") {
      totalAmount += reqBody[i].quantity * prodDetails.price;
      console.log(reqBody[i].quantity);
    }
  }
  return totalAmount;
};

const updateOrderedProduct = async (reqBody, orderId) => {
  for (let i = 0; i < reqBody.length; i++) {
    let prodDetails = await Product.findById(reqBody[i].productId);
    let newOrder = { orderId: orderId, quantity: reqBody[i].quantity };
    prodDetails.supply -= reqBody[i].quantity;
    prodDetails.orders.push(newOrder);
    await prodDetails.save();
  }
};

module.exports.checkSupply = async (reqBody) => {
  try {
    let stocksSupply = {
      withStock: [],
      insufficientStock: [],
    };
    for (let i = 0; i < reqBody.length; i++) {
      let prodDetails = await Product.findById(reqBody[i].productId);
      reqBody[i].currentStoreSupply = prodDetails.supply;
      if (prodDetails.supply > reqBody[i].quantity) {
        stocksSupply.withStock.push(reqBody[i]);
      } else {
        stocksSupply.insufficientStock.push(reqBody[i]);
      }
    }
    return stocksSupply;
  } catch {
    return false;
  }
};

// Business Logic for Check-out

// 1. Check if supply for every product is sufficient.
//      • If not, return the list of sufficient and insufficient.
//      • If all is sufficient, proceed.
// 2. Get the total amount to pay.
// 3. Update products collection, search for product document, subtract the product quantity from the supply, push the transaction details in orders field and save.
// 4. Update users collection, search for the user document and update orders field with the transaction details.

module.exports.checkOutOrder = async (userId, reqBody, buyMode) => {
  try {
    let userDetails = await User.findById(userId);
    //getTotalOrderAmount a function to sum all the orders in 1 transaction

    let totalOrderAmount = 0;
    if (buyMode == "Buy Now") {
      totalOrderAmount = await getTotalOrderAmount(reqBody);
    } else {
      let totArr = await this.getTotalAmountInCart(userId, reqBody);
      totalOrderAmount = totArr.TotalAmount;
    }

    let newOrder = {
      products: reqBody,
      totalAmount: totalOrderAmount,
    };

    let orderTransactionId;
    userDetails.orders.push(newOrder);
    //console.log(userDetails);
    await userDetails.save().then((order) => {
      //push method adds new item in the last index of the array, to get the index of the recently added item, the formula will be length - 1
      orderTransactionId = userDetails.orders[userDetails.orders.length - 1].id;
    });
    //updateOrderedProduct to update the product with the order id and the quantity of the order
    updateOrderedProduct(reqBody, orderTransactionId);
    return true;
  } catch {
    return false;
  }
};

module.exports.updateUserAccess = async (reqParams) => {
  try {
    let userToUpdate = await User.findById(reqParams.userId);
    if (userToUpdate.isAdmin) {
      return "User is already an admin.";
    } else {
      userToUpdate.isAdmin = true;
      await userToUpdate.save();
      return true;
    }
  } catch {
    return false;
  }
};

module.exports.getMyOrders = async (reqId) => {
  try {
    let userOrders = await User.findById(reqId);
    let toShipOrders = userOrders.orders.filter((order) =>
      order.products.some((z) => z.status == "To Ship")
    );

    if (toShipOrders.length > 0) {
      let total = 0;
      if (toShipOrders.length > 1) {
        for (let i = 0; i < toShipOrders.length; i++) {
          total += toShipOrders[i].totalAmount;
        }
      } else {
        total = toShipOrders[0].totalAmount;
        //console.log(total);
      }
      let detailsOfOrders = {
        totalAmount: total,
        orders: toShipOrders,
      };
      return detailsOfOrders;
    } else {
      return "No order to ship. Shop now, hurry!";
    }
  } catch {
    return false;
  }
};

module.exports.getAllOrders = async () => {
  try {
    let allUsers = await User.find({});
    let usersWithOrder = allUsers.filter(
      (user) =>
        user.orders.length > 0 &&
        user.orders.some((z) => z.products.some((x) => x.status == "To Ship"))
    );

    usersWithOrder.map((object) => {
      object.isAdmin = undefined;
      object.password = undefined;
      object.__v = undefined;
      object.cart = undefined;
    });
    return usersWithOrder;
  } catch {
    return false;
  }
};

module.exports.addToCart = async (reqId, reqBody) => {
  try {
    let userDetails = await User.findById(reqId);
    //console.log(userDetails);
    let productInCartIndex = userDetails.cart.findIndex(
      (prod) => prod.productId == reqBody.productId
    );

    if (productInCartIndex < 0) {
      userDetails.cart.push(reqBody);
    } else if (productInCartIndex >= 0) {
      userDetails.cart[productInCartIndex].quantity += 1;
    }
    await userDetails.save();
    return true;
  } catch {
    return false;
  }
};

module.exports.getProductsInMyCart = async (reqId) => {
  try {
    let userDetails = await User.findById(reqId);
    let productsDetails = [];
    for (let i = 0; i < userDetails.cart.length; i++) {
      let prodDetails = await Product.findById(userDetails.cart[i].productId);
      if (prodDetails) {
        productsDetails.push({
          productId: prodDetails._id,
          name: prodDetails.name,
          description: prodDetails.description,
          price: prodDetails.price,
          image: prodDetails.image.url,
          supply: prodDetails.supply,
          quantity: userDetails.cart[i].quantity,
        });
      }
    }
    return productsDetails;
  } catch {
    return false;
  }
};

module.exports.updateProductQtyInCart = async (reqId, reqBody) => {
  try {
    let userDetails = await User.findById(reqId);
    let cartProductIndex = userDetails.cart.findIndex(
      (prod) => prod.productId == reqBody.productId
    );
    userDetails.cart[cartProductIndex].quantity = reqBody.quantity;
    await userDetails.save();
    return true;
  } catch {
    return false;
  }
};

//removeProductFromCart

module.exports.removeProductFromCart = async (reqUserId, reqProductId) => {
  try {
    let userDetails = await User.findById(reqUserId);
    let cartProductIndex = userDetails.cart.findIndex(
      (prod) => prod.productId == reqProductId
    );
    userDetails.cart.splice(cartProductIndex, 1);
    await userDetails.save();
    return true;
  } catch {
    return false;
  }
};

module.exports.getProductAmountInCart = async (reqId, reqBody) => {
  // try {
  let userDetails = await User.findById(reqId);
  let allProdInCart = [];

  let productIndex = 0;
  if (reqBody.length != null) {
    // allProdInCart = reqBody;
    for (let i = 0; i < reqBody.length; i++) {
      productIndex = userDetails.cart.findIndex(
        (z) => z.productId == reqBody[i].productId
      );
      console.log(reqBody[i].productId);
      console.log(productIndex);
      reqBody[i].quantity = userDetails.cart[productIndex].quantity;
    }
    allProdInCart = reqBody;
  } else if (userDetails.cart.length > 0) {
    allProdInCart = userDetails.cart;
  } else {
    return false;
  }

  await allProdInCart;

  let prodInCart = [];
  if (allProdInCart.length > 0) {
    for (let i = 0; i < allProdInCart.length; i++) {
      prodDetails = await Product.findById(allProdInCart[i].productId);
      // console.log(allProdInCart[i].quantity);
      // console.log(prodDetails.price);
      prodInCart.push({
        order: allProdInCart[i],
        unitPricePerItem: prodDetails.price,
        subTotal: prodDetails.price * allProdInCart[i].quantity,
      });
    }
  }
  return prodInCart;
  // } catch {
  //   return false;
  // }
};

module.exports.getTotalAmountInCart = async (reqId, reqBody) => {
  // try {
  let userCartWithSubtotal = await this.getProductAmountInCart(reqId, reqBody);
  let totalAmountInCart = 0;
  console.log(userCartWithSubtotal);
  if (userCartWithSubtotal.length > 0) {
    for (let i = 0; i < userCartWithSubtotal.length; i++) {
      console.log(userCartWithSubtotal[i].subTotal);
      totalAmountInCart += userCartWithSubtotal[i].subTotal;
    }
  }
  return { TotalAmount: totalAmountInCart };
  // } catch {
  //   return false;
  // }
};

module.exports.getQtyFromCart = async (reqId, reqBody) => {
  try {
    let userDetails = await User.findById(reqId);
    let cartProductIndex;
    for (let i = 0; i < reqBody.length; i++) {
      cartProductIndex = userDetails.cart.findIndex(
        (prod) => prod.productId == reqBody[i].productId
      );

      reqBody[cartProductIndex].quantity =
        userDetails.cart[cartProductIndex].quantity;
    }
    return reqBody;
  } catch {
    return false;
  }
};

//getTotalAmountInCart

// module.exports = { checkEmailExists, registerUser };
