const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth.js");

router.get("/productAmountInCart", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  let reqBody;

  // if (req.body.length == null) {
  //   reqBody = null;
  // } else {
  //   reqBody = req.body;
  // }
  // console.log(req.body.length);
  if (!userData.isAdmin) {
    userController
      .getProductAmountInCart(userData.id, req.body)
      .then((resultFromCOntroller) => res.send(resultFromCOntroller));
  } else {
    return res.send(false);
  }
});

router.post("/checkOutFromCart", async (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  const newReqBody = await userController.getQtyFromCart(userData.id, req.body);
  const supply = await userController.checkSupply(newReqBody);
  if (!userData.isAdmin && newReqBody !== false) {
    try {
      if (supply.insufficientStock.length === 0) {
        let result = await userController.checkOutOrderFromCart(
          userData.id,
          newReqBody
        );
        res.send(result);
      } else {
        res.send(resultFromCOntroller);
      }
    } catch {
      res.send("Cannot find one or more product.");
    }
  } else {
    return res.send(false);
  }
});

router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromCOntroller) => res.send(resultFromCOntroller));
});

router.post("/register", (req, res) => {
  userController.checkEmailExists(req.body).then((isRegistered) => {
    if (!isRegistered) {
      userController
        .registerUser(req.body)
        .then((resultFromCOntroller) => res.send(resultFromCOntroller));
    } else {
      return res.send(false);
    }
  });
});

router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromCOntroller) => res.send(resultFromCOntroller));
});

router.get("/details", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  userController
    .getDetails(userData.id)
    .then((resultFromCOntroller) => res.send(resultFromCOntroller));
});

router.post("/checkOutOrder", (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  if (!userData.isAdmin) {
    userController.checkSupply(req.body).then((resultFromCOntroller) => {
      try {
        if (resultFromCOntroller.insufficientStock.length === 0) {
          userController
            .checkOutOrder(userData.id, req.body, "Buy Now")
            .then((result) => res.send(result));
        } else {
          res.send(resultFromCOntroller);
        }
      } catch {
        res.send(false);
      }
    });
  } else {
    return res.send(false);
  }
});

// router.get("/checkSupply", (req, res) => {
//   const userData = auth.decryption(req.headers.authorization);
//   if (!userData.isAdmin) {
//     userController
//       .checkSupply(req.body)
//       .then((resultFromCOntroller) => res.send(resultFromCOntroller));
//   } else {
//     return res.send(false);
//   }
// });

router.patch("/userAccess/:userId", auth.verification, (req, res) => {
  const data = auth.decryption(req.headers.authorization);
  if (data.isAdmin) {
    userController
      .updateUserAccess(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

router.get("/myOrders", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  userController
    .getMyOrders(userData.id)
    .then((resultFromCOntroller) => res.send(resultFromCOntroller));
});

router.get("/allOrders", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  if (userData.isAdmin) {
    userController
      .getAllOrders()
      .then((resultFromCOntroller) => res.send(resultFromCOntroller));
  } else {
    res.send(false);
  }
});

router.put("/addToCart", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  if (!userData.isAdmin) {
    userController
      .addToCart(userData.id, req.body)
      .then((resultFromCOntroller) => res.send(resultFromCOntroller));
  } else {
    res.send(false);
  }
});

router.get("/viewCart", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  if (!userData.isAdmin) {
    userController
      .getProductsInMyCart(userData.id)
      .then((resultFromCOntroller) => res.send(resultFromCOntroller));
  } else {
    res.send(false);
  }
});

router.patch("/changeProductQty", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  if (!userData.isAdmin) {
    userController
      .updateProductQtyInCart(userData.id, req.body)
      .then((resultFromCOntroller) => res.send(resultFromCOntroller));
  } else {
    return false;
  }
});
//removeFromCart

router.delete("/removeFromCart/:productId", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  if (!userData.isAdmin) {
    userController
      .removeProductFromCart(userData.id, req.params)
      .then((resultFromCOntroller) => res.send(resultFromCOntroller));
  } else {
    return res.send(false);
  }
});

router.get("/totalAmountInCart", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  if (!userData.isAdmin) {
    userController
      .getTotalAmountInCart(userData.id, req.body, "Buy From Cart")
      .then((resultFromCOntroller) => res.send(resultFromCOntroller));
  } else {
    return res.send(false);
  }
});

module.exports = router;
