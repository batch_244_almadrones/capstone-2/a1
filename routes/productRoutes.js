const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth.js");

router.get("/orders", auth.verification, (req, res) => {
  const data = auth.decryption(req.headers.authorization);
  if (data.isAdmin) {
    productController
      .getOrdersToShip()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

router.post("/addProduct", auth.verification, (req, res) => {
  const userData = auth.decryption(req.headers.authorization);
  //console.log(userData);
  if (userData.isAdmin) {
    productController
      .addProduct(req.body)
      .then((resultFromCOntroller) => res.send(resultFromCOntroller));
  } else {
    res.send("false");
  }
});

router.get("/allProducts", auth.verification, (req, res) => {
  const data = auth.decryption(req.headers.authorization);
  if (data.isAdmin) {
    productController
      .getAllProducts()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

router.get("/allActiveProducts", (req, res) => {
  productController
    .getAllActiveProducts()
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/:productId", (req, res) => {
  productController
    .getProduct(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

router.put("/updateProduct/:productId", auth.verification, (req, res) => {
  const data = auth.decryption(req.headers.authorization);
  if (data.isAdmin) {
    productController
      .updateProduct(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

router.delete("/archiveProduct/:productId", auth.verification, (req, res) => {
  const data = auth.decryption(req.headers.authorization);
  if (data.isAdmin) {
    productController
      .archiveProduct(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

router.put("/activateProduct/:productId", auth.verification, (req, res) => {
  const data = auth.decryption(req.headers.authorization);
  if (data.isAdmin) {
    productController
      .activateProduct(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

module.exports = router;
