const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes");
const cors = require("cors");
const app = express();
mongoose.connect("", { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.once("open", () =>
  console.log("Now connected to the MongoDB Atlas.")
);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4001, () => {
  console.log(`API is now online at port ${process.env.PORT || 4001}`);
});
