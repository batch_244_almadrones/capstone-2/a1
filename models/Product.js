const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
  name: { type: String, required: true },
  description: {
    type: String,
    required: true,
  },
  category: [{ type: String, required: true }],
  price: { type: Number, required: true },
  isActive: {
    type: Boolean,
    default: true,
  },
  supply: {
    type: Number,
    required: true,
  },
  createdOn: { type: Date, default: new Date() },
  orders: [
    {
      _id: false,
      orderId: String,
      quantity: Number,
      orderedOn: { type: Date, default: new Date() },
    },
  ],
  image: {
    _id: false,
    public_id: { type: String, required: true },
    url: { type: String, required: true },
  },
});

module.exports = mongoose.model("Product", productSchema);
