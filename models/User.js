const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
  firstName: { type: String, required: [true, "First name is required."] },
  lastName: { type: String, required: [true, "Last name is required."] },
  address: String,
  email: { type: String, required: [true, "Email address is required."] },
  password: { type: String, required: [true, "Password is required."] },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: { type: String, required: [true, "Mobile number is required."] },
  orders: [
    {
      products: [
        {
          _id: false,
          productId: String,
          productName: String,
          quantity: Number,
          modeOfPayment: String,
          subtotal: Number,
          status: { type: String, default: "To Ship" },
        },
      ],
      totalAmount: Number,
      purchasedOn: { type: Date, default: new Date() },
    },
  ],
  cart: [
    {
      _id: false,
      productId: String,
      productName: String,
      quantity: { type: Number, default: 1 },
    },
  ],
  wishlist: [
    {
      _id: false,
      productId: String,
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
